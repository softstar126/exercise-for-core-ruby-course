require "FileUtils"

def swap_contents(first_file, second_file)
 FileUtils.mv first_file, "temp.txt"
 FileUtils.mv second_file, first_file
 FileUtils.mv "temp.txt", second_file
end

swap_contents "dest.txt", "src.txt"
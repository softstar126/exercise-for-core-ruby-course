class Rectangle

def initialize(width, length)
 @width = width.to_f
 @length = length.to_f
end

def area
 sprintf("%.2f", @width * @length)
end

def perimeter
 sprintf("%.2f", 2 * ( @width + @length ))
end
end

r = Rectangle.new(23.45, 34.67) 
puts "Area is = #{r.area}"
puts "Perimeter is = #{r.perimeter}"
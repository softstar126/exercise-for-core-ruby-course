(1..100).each do |number|
	fizz_buzz = ''
	fizz_buzz << "Fizz" if number % 3 == 0
	fizz_buzz << "Buzz" if number % 5 == 0
	fizz_buzz << number.to_s if fizz_buzz.empty?
	puts fizz_buzz
end
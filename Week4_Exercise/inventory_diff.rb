def diff_in_two_files(first_txt, second_txt)
 first = File.readlines("../" + first_txt)
 second = File.readlines("../" + second_txt)
 return Proc.new{ second - first} #compare diff between two files
end

added_files = diff_in_two_files("old-inventory.txt", "new-inventory.txt")
removed_files = diff_in_two_files("new-inventory.txt", "old-inventory.txt")

print "Added files are following:\n#{added_files.call.join}\n"
print "Removed files are following:\n#{removed_files.call.join}" 
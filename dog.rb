class Dog
 def initialize(name = "Lucy")
 @name = name
 end

 def bark
  "Ruff! Ruff!"
 end

 def eat
  "I am hungry"
 end

 def chase_cat
  "#{@name} is chasing my cat"
 end
end

d = Dog.new('Leo')

puts d.bark
puts d.eat
puts d.chase_cat
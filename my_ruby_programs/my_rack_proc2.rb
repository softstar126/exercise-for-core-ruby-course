require 'rack'

my_rack_proc = lambda{|env|[200, {"Content-Type" => "text/plain"}, 
["Hi, How are you? The time is #{Time.now}"]]}


Rack::Handler::WEBrick.run my_rack_proc, :Port => 8080
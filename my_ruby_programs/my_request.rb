class MyRequest
 def call(env)
  req = Rack::Request.new(env)
  text = req.params['text']

  Rack::Response.new.finish do |res|
   res['Content-Type'] = 'text/plain' 
   res.status = 200 
   str = "Original string: #{text} V.S Reversed string: #{text.reverse}" 
   res.write str
  end
 end
end
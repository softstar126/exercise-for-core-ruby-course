class Rectangle
 def initialize(width, length)
  @width = width
  @length = length
 end

 def area
  @width * @length
 end

 def perimeter
  2 * ( @width + @length )
 end
end

r1 = Rectangle.new(20.45, 34.67) 
r2 = Rectangle.new(15, 40)
r3 = Rectangle.new(33.17, 17.43)

averages = (r1.area + r2.area + r3.area).to_f / 3 
puts "The average is " + sprintf("%.2f", averages)
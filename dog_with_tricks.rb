class Dog
  attr_accessor :name, :methods

 def initialize(name = "Happy")
  @name = name
  @methods = {}
 end

 def bark
  puts "Ruff! Ruff!"
 end

 def eat
  puts "I am hungry"
 end

 def chase_cat
  puts "#{@name} is chasing my cat"
 end

 def teach_trick(tricks, &command)
  @methods[tricks] = command #store blocks in hash
 end

 def method_missing(tricks)
  #check hash key
  if @methods.has_key?(tricks)
   instance_eval(&methods[tricks])
  else
   "#{@name} doesn't know how to #{tricks}!"
  end
 end
end

d = Dog.new('Lassie')
d.teach_trick(:dance){"#{@name} is dancing!"}
puts d.dance
d.teach_trick(:poo) { "#{@name} is a smelly doggy!" } 
puts d.poo

d2 = Dog.new('Fido')
puts d2.dance 
d2.teach_trick(:laugh) { "#{@name} finds this hilarious!" } 
puts d2.laugh 
puts d.laugh 

d3 = Dog.new('Stimpy') 
puts d3.dance 
puts d3.laugh
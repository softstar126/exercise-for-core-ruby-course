def line_number_alignment(index)
 alignment = line_number_adjust(index).to_s.rjust(3)
end

def line_number_adjust(key)
 (key - 10 < 0) ? "0#{key}" : key
end

s = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nDuis imperdiet sem eu quam.\nDonec bibendum tincidunt purus.\nNunc eu tellus sed turpis volutpat pellentesque.\nNunc accumsan varius elit.\nAenean sit amet magna eget odio ornare vulputate.\nUt ullamcorper tellus non magna.\nSed non arcu vel libero posuere ultricies.\nSuspendisse tincidunt ullamcorper tellus.\nIn ornare lacus ut turpis.\nLorem ipsum dolor sit amet, consectetuer adipiscing elit.\n"

str = s.each_line.with_index(1){|x, key| puts "Line #{line_number_alignment(key)}: #{x}"}
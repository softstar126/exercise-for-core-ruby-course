def leap_year?(year)
 if year % 4 == 0 && year % 100 != 0 || year % 400 == 0
  return true
 else
  return false
 end
end

def minutes_in_a_year(year)
 60 * 24 * (leap_year?(year) ? 366 : 365)
end

[1900, 2006, 2012, 2014 ].each do |year| 
 puts leap_year?(year) ? "#{year} is a leap year and has #{minutes_in_a_year(year)} minutes" : 
 "#{year} is a non-leap year and has #{minutes_in_a_year(year)} minutes"                                           
end 


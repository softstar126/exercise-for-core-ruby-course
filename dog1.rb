class Dog
	attr_accessor :name

	def initialize(name)
		@name = name
	end

	def bark
		puts "Ruff! Ruff!"
	end

	def eat
		puts "I am hungry"
	end

	def chase_cat
		puts "#{@name} is chasing my cat"
	end

	def teach_trick(tricks, &f)
		# f.call(tricks)
		puts instance_eval(&f)
	end

	def dance
		@name 
	end
end

d = Dog.new('Lassie')
d.teach_trick(:dance){"#{@name} is dancing!"}
puts d.dance
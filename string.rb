def split_str(str)
 str.each_line.with_index(1) do |value, key|
  yield value, key
 end
end
s = "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"

split_str(s){|value, key| puts "Line #{key}: #{value}"}
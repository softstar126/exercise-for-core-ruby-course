def save_game(file)
 score = 1000
 open(file, "w") do |f|
 f.puts(score)
 f.puts(Time.new.to_i)
 end
end

def add_keys(result_collection)
 saved_result = {}
 keys = [:score, :saved_time] #store keys in an array
 result_collection.each_with_index do |value, key|
  saved_result[keys[key]] = result_collection[key].to_i
 end
 return saved_result
end

def load_game(file)
 #store each line in an array
 saved_result_collect = File.open("game.sav", 'r').each_line.collect do |value| 
  value.gsub("\n", '')
 end

 saved_result_with_keys = add_keys(saved_result_collect)
 
 #check value in hash, if the value exist, display score. If no, raise a runtime error
 if saved_result_with_keys.has_value?(File.mtime(file).to_i)
  "your saved score is #{saved_result_with_keys[:score]}"
 else
  raise "I suspect you of cheating."
 end
	
end

save_game("game.sav")

sleep(2)

puts load_game("game.sav") # => "Your saved score is 1000."

# Now let's cheat by increasing our score to 9000

open("game.sav", "r+b") { |f| f.write("9") }

load_game("game.sav") # RuntimeError: I suspect you of cheating.

def id3_tag_analyzer(mp3_file)
 #store fields name and bytes in a hash
 fields_and_bytes = {
  :Song_Title => 30, 
  :Artist => 30, 
  :Album => 30,
  :Year => 4,
  :Comment => 30, 
  :Genre => 1 }

 fields = {}
  File.open('song.mp3') do |f|
   f.seek(-128, IO::SEEK_END)
    if f.read(3) == "TAG"
     fields_and_bytes.each do |field, bytes|
      data = f.read(bytes).gsub(/\x00/, '')
      fields[field] = data
     end
    end
  end
  fields
end

id3_tag_analyzer('song.mp3').each{|field, content| puts "#{field}: #{content}" }
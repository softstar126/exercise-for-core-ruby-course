def line_number_alignment(line_number, number_length)
 line_number_adjust(line_number.max).to_s.rjust(number_length.to_s.length)
end

def line_number_adjust(key)
 (key - 10 < 0) ? "0#{key}" : key
end

def count_lines(str)
 str.each_line.count
end

s = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nDuis imperdiet sem eu quam.\nDonec bibendum tincidunt purus.\nNunc eu tellus sed turpis volutpat pellentesque.\nNunc accumsan varius elit.\nAenean sit amet magna eget odio ornare vulputate.\nUt ullamcorper tellus non magna.\nSed non arcu vel libero posuere ultricies.\nSuspendisse tincidunt ullamcorper tellus.\nIn ornare lacus ut turpis.\nLorem ipsum dolor sit amet, consectetuer adipiscing elit.\n"

line_number_collections = []

s.each_line.with_index(1) do |content, line_number|
 line_number_collections << line_number
 amount_of_lines = line_number_collections.max + count_lines(s) - 1 
 printf "Line #{line_number_alignment(line_number_collections, amount_of_lines)}: #{content}"
end
def Repeat_call(interval_time)
 total_of_calls = {
 1 => 'First',
 2 => 'Second',
 3 => 'Third',
 4 => 'Fourth',
 5 => 'Fifth',
 6 => 'Sixth',
 7 => 'Seventh',
 8 => 'Eighth',
 9 => 'Ninth',
 10 => 'Tenth'
}
 times_of_repeat = 0

 total_of_calls.each do |key, vlaue|
  sleep(interval_time)
  times_of_repeat += 1
  if block_given?
   yield total_of_calls[times_of_repeat]
  else
   raise "Block is not given"
  end

 end

end

Repeat_call(5){|times| puts "#{times} call"}
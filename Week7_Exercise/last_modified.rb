def last_modified(file)
 if File.exists?(file)
  current_time = Time.now
  file_modified_time = File.mtime(file)
  (current_time - file_modified_time) / (24 * 60 * 60)
 else
  raise "There is no such file"
 end
end

printf("The file was last modified %.12f days ago", last_modified(__FILE__))

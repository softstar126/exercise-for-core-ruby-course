class GameBoard
 attr_accessor :set_locations_cells, :no_of_hits

 def initialize
  @no_of_hits = 0 #initialize no_of_hits
 end

 def set_locations_cells(cell_locations)
  @cells = cell_locations
 end

 def check_yourself(user_guess)
  if @cells.include?(user_guess.to_i) 
   @no_of_hits += 1
   if @no_of_hits == 3
    print "End\n" 
    return 'kill'
   else 
    print "Hit\n"
   end
  else
   print "Miss\n"
  end
 end
end
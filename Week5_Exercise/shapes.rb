class Gui
 def initialize(sound)
  @sound = sound
 end

 def click
  "#{rotate} and #{sound}"
 end

 def rotate
  "rotates 360 degrees"
 end

 def sound
  "plays AIF sound"
 end
end

class Square < Gui
 def sound
  "plays #{@sound} sound"
 end
end

class Circle < Gui
 def sound
  "plays #{@sound} sound"
 end
end

class Triangle < Gui
 def sound
  "plays #{@sound} sound"
 end
end

square = Square.new("square.aif")
puts square.click

circle = Circle.new("circle.aif")
puts circle.click

triangle = Triangle.new("triangle.aif")
puts triangle.click
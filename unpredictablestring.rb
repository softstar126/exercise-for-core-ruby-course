class UnpredictablrString < String
 attr_accessor :scramble

 def initialize(str)
  @str = str
  super
 end

 def scramble
  @str.each_char.to_a.shuffle.join
 end
end

shuffled_string = UnpredictablrString.new("It was a dark and stormy night.")
print shuffled_string.scramble